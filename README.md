# CSS Slider
A dead simple slider using a minimal set of css and no javascript

inspired by https://markus.oberlehner.net/blog/super-simple-progressively-enhanced-carousel-with-css-scroll-snap/

# More modules building on CSS scroll behavior

- https://www.drupal.org/project/simple_carousel
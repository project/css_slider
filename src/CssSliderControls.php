<?php

namespace Drupal\css_slider;

use Drupal\Core\Url;

class CssSliderControls
{
    public static function create(array $slides): array
    {
        $controls = [];

        foreach ($slides as $slide) {
            $title = [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => $slide['#label'],
                '#attributes' => ['class' => ['visually-hidden']]
            ];

            $controls[] = [
                '#type' => 'link',
                '#title' => $title,
                '#url' => Url::fromUserInput('#' . $slide['#id']),
                '#attributes' => ['class' => ['dot']]
            ];
        };

        return $controls;
    }
}

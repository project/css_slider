<?php

declare(strict_types=1);

namespace Drupal\css_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

use Drupal\css_slider\CssSliderFormatterTrait;
use Drupal\css_slider\CssSliderControls;

/**
 * Plugin implementation of the 'CssSliderFormatter' formatter.
 */
#[FieldFormatter(
  id: "image_css_slider",
  label: new TranslatableMarkup("CSS Slider"),
  field_types: [
    'image',
  ],
)]
class CssSliderImageFormatter extends ImageFormatter {
  use CssSliderFormatterTrait;
}

<?php

declare(strict_types=1);

namespace Drupal\css_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

use Drupal\css_slider\CssSliderFormatterTrait;
use Drupal\css_slider\CssSliderControls;

/**
 * Plugin implementation of the 'CssSliderFormatter' formatter.
 */
#[FieldFormatter(
  id: "entity_reference_css_slider",
  label: new TranslatableMarkup("CSS Slider"),
  field_types: [
    'entity_reference',
  ],
)]
class CssSliderEntityReferenceFormatter extends EntityReferenceEntityFormatter {
  use CssSliderFormatterTrait;
}

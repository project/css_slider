<?php

declare(strict_types=1);

namespace Drupal\css_slider\Plugin\views\style;

use Drupal\views\Attribute\ViewsStyle;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Css Slider style plugin.
 * 
 * @@ingroup views_style_plugins
 */
#[ViewsStyle(
  id: "css_slider",
  title: new TranslatableMarkup("Css Slider"),
  help: new TranslatableMarkup("Display rows as a slider."),
  theme: "css_slider",
  display_types: ["normal"],
)]
final class CssSliderStyle extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  // protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['controls'] = ['default' => TRUE];
    $options['axis'] = ['default' => 'inline'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['axis'] = [
      '#type' => 'select',
      '#title' => $this->t('Axis'),
      '#default_value' => $this->options['axis'],
      '#options' => [
        'inline' => $this->t('Inline'),
        'block' => $this->t('Block'),
        'both' => $this->t('Both'),
      ],
      '#weight' => 10,
    ];

    $form['controls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Controls'),
      '#default_value' => $this->options['controls'],
      '#weight' => 11,
    ];
  }
}

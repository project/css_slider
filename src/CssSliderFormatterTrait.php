<?php

declare(strict_types=1);

namespace Drupal\css_slider;

use Drupal\Core\Template\Attribute;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

trait CssSliderFormatterTrait {
  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->isMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Axis: @axis', [
      '@axis' => ucfirst($this->getSetting('axis')),
    ]);
    $summary[] = $this->t('Controls: @controls', [
      '@controls' => $this->getSetting('controls') ? $this->t('On') : $this->t('Off')
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'axis' => 'inline',
      'controls' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Add the optionset setting.
    // $element = $this->buildSettingsForm();
    $element = [];

    // Add the image settings.
    $element = array_merge($element, parent::settingsForm($form, $form_state));
    // We don't need the link setting.
    $element['image_link']['#access'] = FALSE;

    $element['axis'] = [
      '#type' => 'select',
      '#title' => $this->t('Axis'),
      '#default_value' => $this->getSetting('axis'),
      '#options' => [
        'inline' => $this->t('Inline'),
        'block' => $this->t('Block'),
        'both' => $this->t('Both'),
      ],
      '#weight' => 10,
    ];

    $element['controls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Slider Controls'),
      '#default_value' => $this->getSetting('controls'),
      '#weight' => 11,
    ];

    // Add the caption setting.
    // if (!empty($this->getSettings())) {
    //   $element += $this->captionSettings($this, $this->fieldDefinition);
    // }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $entities = $this->getEntitiesToView($items, $langcode);

    $build = [
      '#theme' => 'css_slider',
      '#axis' => $this->getSetting('axis'),
      '#attributes' => new Attribute(),
      '#attached' => ['library' => ['css_slider/css_slider']]
    ];

    foreach ($elements as $delta => $element) {
      $entity = $entities[$delta];

      $build['#slides'][] = [
        '#label' => $entity->label(),
        '#id' => 'slide-' . $entity->getEntityTypeId() . '-' . $entity->id(),
        'content' => $element,
      ];
    }

    if($this->getSetting('controls')) {
      $build['#controls'] = CssSliderControls::create($build['#slides']);
      $build['#attached']['library'][] = 'css_slider/controls';
    }


    return $build;
  }
}
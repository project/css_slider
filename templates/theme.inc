<?php

declare(strict_types=1);

use Drupal\css_slider\CssSliderControls;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for css-slider.html.twig template.
 */
function template_preprocess_css_slider(array &$variables): void
{
  if(isset($variables['view'])) {
    $view = $variables['view'];
    
    foreach($variables['rows'] as $delta => $row) {
      $entity = $view->result[$delta]->_entity;

      $variables['slides'][$delta] = [
        '#label' => $entity->label(),
        '#id' => 'slide-' . $entity->getEntityTypeId() . '-' . $entity->id(),
        'content' => $row,
      ];
    }

    $variables['#attached']['library'][] = 'css_slider/css_slider';

    $options = $view->style_plugin->options;
    $variables['axis'] = $options['axis'];
    
    if($options['controls']) {
      $variables['controls'] = CssSliderControls::create($variables['slides']);
      $variables['#attached']['library'][] = 'css_slider/controls';
    }
  }
  
  if (isset($variables['slides'])) {
    foreach ($variables['slides'] as $delta => $slide) {
      $variables['slides'][$delta] = [
        'content' => $slide,
        'attributes' => new Attribute([
          'id' => $slide['#id'],
          'class' => 'slide'
        ]),
      ];
    }
  }
}